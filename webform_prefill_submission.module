<?php

/**
 * @file
 * This module allows users to create new webform submissions from the data of previous ones.
 *
 * This module is written by Bismarck Fuentes for the Information Systems for Investigations and 
 * Graduate Studies Department at Monterrey Institute of Technology and Higher Studies (ITESM).
 *
 */

/**
 * Implementation of hook_help
 */
function webform_prefill_submission_help($path) {
  switch ($path) {
    case 'admin/modules#description':
      return t('Allows users to create new webform submissions from the data of previous ones.');
      break;
  }
}


/**
 * Implementation of hook_permission
 */
function webform_prefill_submission_permission() {
  return array(
    'prefill from all webform submissions' => array(
      'title' => t('Prefill from all Webform Submissions'),
	  'description' => t('Allows prefilling from any webform submission by any user. Generally an administrative permission.')),
	'prefill from own webform submissions' => array(
      'title' => t('Prefill from own Webform Submissions')
    )
  );
}//function webform_prefill_submission_permission()


/**
 * Prefill access function
 */
function webform_prefill_submission_access($node, $submission, $op = 'view', $account = NULL) {
  global $user;
  
  $account = isset($account) ? $account : $user;

  $access_all = user_access('prefill from all webform submissions', $account);
  $access_own_submission = true;
  //isset($submission) && user_access('prefill from own webform submissions', $account)&&(($account->uid && $account->uid == $submission->uid) || isset($_SESSION['webform_submission'][$submission->sid]));
  $access_node_submissions = user_access('access own webform results', $account) && $account->uid == $node->uid;

  $general_access = $access_all || $access_own_submission || $access_node_submissions;

  // Disable the page cache for anonymous users in this access callback,
  // otherwise the "Access denied" page gets cached.
  if (!$account->uid && user_access('access own webform submissions', $account)) {
    webform_disable_page_cache();
  }

 $module_access = count(array_filter(module_invoke_all('webform_submission_access', $node, $submission, $op, $account))) > 0;

      return $module_access || ($general_access && (user_access('prefill from all webform submissions', $account) || (user_access('prefill from own webform submissions', $account) && $account->uid == $submission->uid)));

  return true;
}
//webform_prefill_submission_access



/**
 * Implementation of hook_menu()
 */
function webform_prefill_submission_menu() {

  $items = array();

  $items['node/%webform_menu/submission/%webform_menu_submission/prefill'] = array(
    'title' => 'Prefill new',
    'load arguments' => array(1),
    'page callback' => 'webform_clone',
    'page arguments' => array(1, 3),
    'access callback' => 'webform_prefill_submission_access',
    'access arguments' => array(1, 3, 'prefill'),
    'weight' => 1,
    'file' => 'includes/webform.clone.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}//function webform_prefill_submission_menu()


/**
 * Implementation of hook_theme
 */
// We don't really use a theme here, but we need to implement it
// in order to add the prefill link to the submissions page
function webform_prefill_submission_theme() {
  return array();
} // function webform_prefill_submission_theme()




/**
 * Adds a prefill link to the webform submission page.
 */
 
function webform_prefill_submission_preprocess_webform_results_submissions(&$vars) {
  $rows =& $vars['table']['#rows'];

  $i = 0;
  $table_row_keys = array_keys($vars['table']['#rows']);
  foreach ($vars['submissions'] as $sid => $submission) {
    if (webform_prefill_submission_access($vars['node'], $submission)) {
      $rows[$table_row_keys[$i]][] = l(t('Prefill new'), 'node/' . $submission->nid . '/submission/' . $submission->sid . '/prefill');
    }
    else {
      $rows[$table_row_keys[$i]][] = '';
    }
    $i++;
  }

  $last_header_key = array_pop(array_keys($vars['table']['#header']));
  $vars['table']['#operation_total'] += 1;
  $vars['table']['#header'][$last_header_key]['colspan'] += 1;
}

