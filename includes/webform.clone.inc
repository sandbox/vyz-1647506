<?php

/**
 * @file
 * This file contains the actual cloning function
 *
 * @author Bismarck Fuentes <jo333fm@gmail.com>
 */

/**
 * 1.Loads the current submission and clones it into the database with a  
 * 	different sid and time
 * 2.Sends the user to the form in 'Draft' mode.
 */

 function webform_clone($node, $submission) {
 	unset($submission->sid);
	$submission->submitted=time();
	$submission->is_draft=1;
 	module_load_include('inc', 'webform', 'includes/webform.submissions');
	webform_submission_insert($node, $submission);
	$path='node/' . $submission->nid;
	drupal_goto($path, array(), $http_response_code = 301);
 }